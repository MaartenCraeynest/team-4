Uitleg over librairies: 

Librairies zijn voorgecodeerde bestanden die specifieke taken verrichten. Deze zijn geprogrammeerd in C++. Een librairy bestaat meestal uit een *.h bestand, de header, en een *.cpp bestand. Beide zijn vereist voor een correcte werking. Het kan zijn dat er nog andere bestanden bijgevoegd worden, meestal onder de vorm van assets of voorbeelden. 

Om een librairy toe te voegen maak je een niewe map onder */arduino/librairies/ met dezelfde naam als de librairy die je wil toevoegen. Daar voeg je alle nodige bestanden toe.
Vervolgens kan je de code laden door het #include <*.h> commando in het begin van de code te zetten.
HET VOLSTAAT NIET DAT DE LIBRAIRIES IN DE GIT MAP STAAN. Ze moeten in je eigen arduino map staan. (Vaak in mijn documenten).

Errors onder de vorm van "*h missing" zijn vaak omdat de librairy files op de verkeerde plaats zijn opgeslaan.


Meer info over het installeren van bibliotheken op: http://arduino.cc/en/Guide/Libraries
