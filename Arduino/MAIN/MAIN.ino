#include <QTRSensors.h>

//Digitale Poorten   
int MotorL[] = {5,6};                     
int MotorR[] = {9,10};
//Analoge Poorten
int AS = A0;


//Variabelen voor lijnsensor
const int LS = 8;
QTRSensorsRC qtr((unsigned char[]){2,3,4,7,8,11,12,13}, LS);
const bool  WHITELINE = false;



void setup() {
  //Zet Baudrate voor Seriële comminucatie
  Serial.begin(9600);
  
  for (int i = 0; i < sizeof(MotorL)-1; i++){
    pinMode(MotorL[i], INPUT);
  }
  
  for (int i = 0; i < sizeof(MotorR)-1; i++){
    pinMode(MotorR[i], INPUT);
  }
    
  pinMode(AS, INPUT);  
  
  
    Serial.write("Begin Callibratie");
  //Calibreert lijn sensor
  for ( int i = 0; i < 250; i++){
    qtr.calibrate();
    delay(20);
   
  }

}

//belangrijkste programmaloop
void loop() {
  //Controleer op obstakels
  if(hindernis()){
    //Stop beide motoren
    quickStop();
  }
  else 
  {
    lijnDetectie();
    
   }
    
   
}



//Algemene functie die beide motoren in beide richtingen bestuurd.
//Gebruik [-1,0] voor achteruit te rijden.
//        [0,1] om vooruit te rijden.
//Meegegeven getallen bepalen stuwkracht.
void rijden(double pL, double pR){
  //Controle dat meegegeven parameters correct zijn.
  if ( pL*pL <=1 && pR*pR <=1){
    //Zet de parameter om naar een waarde tussen 0-255 (analoge waarden)
    pL =  pL * 255;
    pR =  pR * 255;
    
    //Controleerd de 4 verschillende opties:
     if (pL > 0){
      digitalWrite(MotorL[0],LOW);
      analogWrite(MotorL[1],pL);
      Serial.print("Linkse motor: " );
      Serial.println(pL);
    }
    else if(pL<=0){
      pL = -pL; 
      analogWrite(MotorL[0],pL);
      digitalWrite(MotorL[1],LOW);
      Serial.print("Linkse motor: " );
      Serial.println(pL);
     }
    if (pR > 0){
      digitalWrite(MotorR[0],LOW);
      analogWrite(MotorR[1],pR);
      Serial.print("Rechtse motor: ");
      Serial.println(pR);
     }
     else if(pR<=0){
      pR = -pR;
      analogWrite(MotorR[0],pR);
      digitalWrite(MotorR[1],LOW);
      Serial.print("Rechtse motor: ");
      Serial.println(pR);
     }
    

   }
      
}
  
//Standaardoptie om rechtdoor te rijden.
void rechtdoor(){
  analogWrite(MotorL[0],150);
  analogWrite(MotorL[1],255);
  analogWrite(MotorR[0],150);
  analogWrite(MotorR[1],255);
}
//Standaardoptie om achteruit te rijden.
void achteruit(){
  analogWrite(MotorL[0],255);
  analogWrite(MotorL[1],100);
  analogWrite(MotorR[0],255);
  analogWrite(MotorR[1],100);
}

//Snelle stop (actief remmen).
void quickStop(){
  digitalWrite(MotorL[0],HIGH);
  digitalWrite(MotorL[1],HIGH);
  digitalWrite(MotorR[0],HIGH);
  digitalWrite(MotorR[1],HIGH);
}

//Trage stop (uitbollen).
void slowStop(){
  digitalWrite(MotorL[0],HIGH);
  digitalWrite(MotorL[1],HIGH);
  digitalWrite(MotorR[0],HIGH);
  digitalWrite(MotorR[1],HIGH);
}

//Leest de afstandsensor af en bepaald of het nodig is om te stoppen.
bool hindernis(){
   int sensorValue = analogRead(AS);
   // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5.0 / 1023.0);
  Serial.println(voltage);
  if(voltage > 1.5)
    return true;
  else
    return false;
}

void lijnDetectie(){
    // put your main code here, to run repeatedly:
  unsigned int sensors[8];
  
  int position  = qtr.readLine(sensors,QTR_EMITTERS_ON, WHITELINE);
  //&& sensors[4] > 0
  if (sensors[0] > 0 && sensors[1] > 0 && sensors[2] > 0 && sensors[3] > 0  && sensors[5] > 0 && sensors[6] > 0 && sensors[7] >0 && !WHITELINE){
    quickStop();
    Serial.println("STOP");
    return;
    }
    
  else if (sensors[0] < 0 && sensors[1] < 0 && sensors[2] < 0 && sensors[3] < 0 && sensors[4] < 0 && sensors[5] < 0 && sensors[6] < 0 && sensors[7] < 0 && WHITELINE){
    quickStop();
    Serial.println("STOP");
    return;
    }  
  
  else {
  double error = position - (LS-1)*500;
  error = error /3500;
  
  if (error > 0.6){
    error = 0.6;
   }
  if (error < -0.6){
    error = -0.6;
   }
  
  if (error > 0 ){
      rijden(1 - 1.6*error,1);
  }
  if (error < 0 ){
      rijden(1, 1 + 1.6*error);
  }
      
    Serial.println(error);
    Serial.print("1 : ");
    Serial.println(sensors[0]);
    Serial.print("2 : ");
    Serial.println(sensors[1]);
    Serial.print("3 : ");
    Serial.println(sensors[2]);
    Serial.print("4 : ");
    Serial.println(sensors[3]);
    Serial.print("5 : ");
    Serial.println(sensors[4]);
    Serial.print("6 : ");
    Serial.println(sensors[5]);
    Serial.print("7 : ");
    Serial.println(sensors[6]);
    Serial.print("8 : ");
    Serial.println(sensors[7]);
  }
}




