//Digitale Poorten
int SA[] = {2,3,4,7,8,11,12,13};         
int MotorL[] = {5,6};                     
int MotorR[] = {9,10};
//Analoge Poorten
int AS = A0;



void setup() {
  //Zet Baudrate voor Seriële comminucatie
  Serial.begin(9600);
  //Initializeren van alle poorten
  for (int i = 0; i < sizeof(SA)-1; i++){
    pinMode(SA[i], OUTPUT);
  }
  
  for (int i = 0; i < sizeof(MotorL)-1; i++){
    pinMode(MotorL[i], INPUT);
  }
  
  for (int i = 0; i < sizeof(MotorR)-1; i++){
    pinMode(MotorR[i], INPUT);
  }
    
  pinMode(AS, INPUT);  
  
  

}

//belangrijkste programmaloop
void loop() {
  //Controleer op obstakels
  if(hindernis()){
    //Stop beide motoren
    quickStop();
  }
  else 
  {
    //Main program
    rijden(0.3,-0.3);
    delay(1000);
   }
    
   
}

//Algemene functie die beide motoren in beide richtingen bestuurd.
//Gebruik [-1,0] voor achteruit te rijden.
//        [0,1] om vooruit te rijden.
//Meegegeven getallen bepalen stuwkracht.
void rijden(double pL, double pR){
  //Controle dat meegegeven parameters correct zijn.
  if ( pL*pL <=1 && pR*pR <=1){
    //Zet de parameter om naar een waarde tussen 0-255 (analoge waarden)
    pL =  pL * 255;
    pR =  pR * 255;
    
    //Controleerd de 4 verschillende opties:
     if (pL > 0){
      digitalWrite(MotorL[0],LOW);
      analogWrite(MotorL[1],pL);
      Serial.print("Linkse motor: " );
      Serial.println(pL);
    }
    else if(pL<=0){
      pL = -pL; 
      analogWrite(MotorL[0],pL);
      digitalWrite(MotorL[1],LOW);
      Serial.print("Linkse motor: " );
      Serial.println(pL);
     }
    if (pR > 0){
      digitalWrite(MotorR[0],LOW);
      analogWrite(MotorR[1],pR);
      Serial.print("Rechtse motor: ");
      Serial.println(pR);
     }
     else if(pR<=0){
      pR = -pR;
      analogWrite(MotorR[0],pR);
      digitalWrite(MotorR[1],LOW);
      Serial.print("Rechtse motor: ");
      Serial.println(pR);
     }
    

   }
      
}
  
//Standaardoptie om rechtdoor te rijden.
void rechtdoor(){
  analogWrite(MotorL[0],150);
  analogWrite(MotorL[1],255);
  analogWrite(MotorR[0],150);
  analogWrite(MotorR[1],255);
}
//Standaardoptie om achteruit te rijden.
void achteruit(){
  analogWrite(MotorL[0],255);
  analogWrite(MotorL[1],100);
  analogWrite(MotorR[0],255);
  analogWrite(MotorR[1],100);
}

//Snelle stop (actief remmen).
void quickStop(){
  digitalWrite(MotorL[0],HIGH);
  digitalWrite(MotorL[1],HIGH);
  digitalWrite(MotorR[0],HIGH);
  digitalWrite(MotorR[1],HIGH);
}

//Trage stop (uitbollen).
void slowStop(){
  digitalWrite(MotorL[0],HIGH);
  digitalWrite(MotorL[1],HIGH);
  digitalWrite(MotorR[0],HIGH);
  digitalWrite(MotorR[1],HIGH);
}

//Leest de afstandsensor af en bepaald of het nodig is om te stoppen.
bool hindernis(){
   int sensorValue = analogRead(AS);
   // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue * (5.0 / 1023.0);
  Serial.println(sensorValue);
  if(voltage > 1.5)
    return true;
  else
    return false;
}




